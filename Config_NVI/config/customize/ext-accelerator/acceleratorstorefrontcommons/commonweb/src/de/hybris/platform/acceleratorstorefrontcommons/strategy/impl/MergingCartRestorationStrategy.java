package de.hybris.platform.acceleratorstorefrontcommons.strategy.impl;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.order.CommerceCartMergingException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import org.springframework.beans.factory.annotation.Autowired;

public class MergingCartRestorationStrategy extends DefaultCartRestorationStrategy
{
	private static final Logger LOG = Logger.getLogger(MergingCartRestorationStrategy.class);
	@Autowired
	private CommerceCartService commerceCartService;
	@Autowired
	private BaseSiteService baseSiteService;
	@Autowired
	private UserService userService;
	@Autowired
	private ModelService modelService;


	@Override
	public void restoreCart(final HttpServletRequest request)
	{
		// no need to merge if current cart has no entry
		if (!getCartFacade().hasEntries())
		{
			super.restoreCart(request);
		}
		else
		{
			final String sessionCartGuid = getCartFacade().getSessionCartGuid();
			final BaseSiteModel currentBaseSite =getBaseSiteService().getCurrentBaseSite();
			final CartModel fromCart = getCommerceCartService().getCartForGuidAndSiteAndUser(sessionCartGuid, currentBaseSite,
					getUserService().getCurrentUser());
			final String mostRecentSavedCartGuid = getMostRecentSavedCartGuid(sessionCartGuid);
			if (StringUtils.isNotEmpty(mostRecentSavedCartGuid))
			{
				getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
				try
				{
					getSessionService().setAttribute(WebConstants.CART_RESTORATION,
							getCartFacade().restoreCartAndMerge(mostRecentSavedCartGuid, sessionCartGuid));
					request.setAttribute(WebConstants.CART_MERGED, Boolean.TRUE);
				}
				catch (final CommerceCartRestorationException e)
				{
					if (LOG.isDebugEnabled())
					{
						LOG.debug(e);
					}
					getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
							WebConstants.CART_RESTORATION_ERROR_STATUS);
				}
				catch (final CommerceCartMergingException e)
				{
					LOG.error("User saved cart could not be merged", e);
				}
			}
			else
			{
				if (fromCart != null)
				{
					for (final AbstractOrderEntryModel entry : fromCart.getEntries())
					{
						entry.setPatient(null);
						getModelService().save(entry);
					}
				}
			}
		}
	}

	/**
	 * Determine the most recent saved cart of a user for the site that is not the current session cart. The current
	 * session cart is already owned by the user and for the merging functionality to work correctly the most recently
	 * saved cart must be determined. getMostRecentCartGuidForUser(excludedCartsGuid) returns the cart guid which is
	 * ordered by modified time and is not the session cart.
	 *
	 * @param currentCartGuid
	 * @return most recently saved cart guid
	 */
	protected String getMostRecentSavedCartGuid(final String currentCartGuid)
	{
		return getCartFacade().getMostRecentCartGuidForUser(Arrays.asList(currentCartGuid));
	}

	public CommerceCartService getCommerceCartService() {
		return commerceCartService;
	}

	public void setCommerceCartService(CommerceCartService commerceCartService) {
		this.commerceCartService = commerceCartService;
	}

	public BaseSiteService getBaseSiteService() {
		return baseSiteService;
	}

	public void setBaseSiteService(BaseSiteService baseSiteService) {
		this.baseSiteService = baseSiteService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public ModelService getModelService() {
		return modelService;
	}

	public void setModelService(ModelService modelService) {
		this.modelService = modelService;
	}
}

