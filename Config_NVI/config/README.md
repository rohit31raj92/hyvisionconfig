### setup instructions ###

This repo contains all the configurations that are required for **nvi b2c implementation**, compatible with hybris 6.1
setup

* clone this repo into hybris/config/
* Setup extensions as per [nvi-b2c-ext](https://bitbucket.org/pragiti-git/nvi-b2c-ext) repo

### contribution guidelines ###

Please make sure to add JIRA ticket number as a part of all commit messages
examples

* [NVI-10] intial readme setup
* [NVI-10, NVI-9] git setup